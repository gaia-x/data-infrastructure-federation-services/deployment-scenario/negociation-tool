const cors = require('cors');
const express = require('express');
const serviceController = require('./controllers/serviceController');

/* For local development */
if (process.env.NODE_ENV !== "production") require("dotenv").config();

/* Express Configuration */
const PORT = 3002; 
const app = express();
app.use(express.json());
app.use(cors());

/* Database creation */
const { init } = require("./service/mongodb");
(async ()=> {
    await init();
})();

/* Routes */
app.post('/subscribe', serviceController.subscribe);

app.post('/negotiate', serviceController.negotiate);

app.post('/signature', serviceController.startSignature);

app.post('/extract-odrl-variables', serviceController.extractODRLVariables);

app.post('/extract-odrl-data', serviceController.extractODRLData);


/* Error */
app.use((error, req, res, next) => {
    if (error instanceof Error) {
        console.log(error.message);
        res.status(500).json({ error: error.message });
    } else {
        res.status(500).json({ error: 'Internal server error' });
    }
});

/* Start Express Server */
app.listen(PORT, () => {
    console.log(`Server starting on port ${PORT}`)
});