const { connectDatabase, closeDatabase } = require('../configuration/mongobase')
const utils = require('../utils/utils');
const config = require('../configuration/config');

async function verifyTemplateData(req, res, next) {
  try {
      console.log("#### Start verify template ####")
      const db = await connectDatabase(config.dbName);
      const collection = await db.collection(config.collectionNameData);
      const dataTemplate = await collection.findOne({}, { projection: { _id: 0 }} );
      utils.compareJSONWithTemplate(req.body, dataTemplate);
      console.log("#### End verify Template ####")
      next()
  } catch (error) {
      next(new Error(error));
    } finally {
      await closeDatabase();
    }
}

module.exports = {
  verifyTemplateData
}