const logger = require("../service/logger");
const mongoService = require('../service/mongodb');
const mailjetService = require('../service/mailjet');
const verifierService = require('../service/verifier');
const keycloakService = require('../service/keycloak');
const contractService = require('../service/contract');
const nextcloudService = require('../service/nexcloud');
const gatewaySignatureService = require('../service/gatewaySignature');
const utils=require('../Utils/utils')


const CONTRACT_NAME = "terms_and_conditions";
const NEGOTIATION_FOLDER = "NegociationService";

// Souscription à un service
exports.subscribe = async (req, res, next) => {
    try {
        /* Verify the credential (signature, credentialStatus, ...) */
        await verifierService.verifyCredential(req.body.VerifiableCredential);

        /* Get the contract Template */
        const pathContractTemplate = `/Template_Contract/${req.body.Data["gx:providedBy"]}/${CONTRACT_NAME}.docx`;
        const contract = await nextcloudService.getFileFromNextcloud(pathContractTemplate);

        /* Get ODRL */
        const odrl = await contractService.fetchODRL(req.body.Data["gx:termsAndConditions"]);

        /* Update the contract with the odrl and convert it in PDF format */
        const termsAndConditionWord = await contractService.updateContractFromODRL(contract, odrl);
        const termsAndConditionPdf = await contractService.convertWordToPDF(termsAndConditionWord);

        /* Check if user exists on keycloak and get user Id */
        const userIdConsumer = await keycloakService.getUserKeycloakId(req.body.Consumer.email);
        const userIdProvider = await keycloakService.getUserKeycloakId(req.body.Provider.email);
        if (!userIdConsumer.status || !userIdProvider.status) {
            throw new Error("The Consumer or Provider does not exist on keycloak.");
        }

        /* Build signatories data */
        const dataConsumer = {
            "gx-signature:userid": userIdConsumer,
            "gx-signature:email": req.body.Consumer.email,
            "gx-signature:did": req.body.Consumer.username,
            "gx-signature:firstname": req.body.Consumer.firstName,
            "gx-signature:lastname": req.body.Consumer.lastName,
            "gx-signature:mobile": "0646273476"
        }

        const dataProvider = {
            "gx-signature:userid": userIdProvider,
            //"gx-signature:email": req.body.Provider.email,
            "gx-signature:email": req.body.Consumer.email,
            "gx-signature:did": req.body.Provider.username,
            "gx-signature:firstname": req.body.Provider.firstName,
            "gx-signature:lastname": req.body.Provider.lastName,
            "gx-signature:mobile": "0646273476"
        }

        /* Start signature */
        await gatewaySignatureService.beginSignature(termsAndConditionPdf, `${CONTRACT_NAME}.pdf`, [dataProvider, dataConsumer], odrl, req.body.VerifiableCredential)

        res.status(200).json({ message: 'Successful subscription' });
    }
    catch (error) {
        next(new Error(`Error to subcribe : ${error}`));
    }
};

// Négociation d'un service
exports.negotiate = async (req, res, next) => {
    try {
        /* Verify the credential (signature, credentialStatus, ...) */
        await verifierService.verifyCredential(req.body.VerifiableCredential);
        
        /* Get the contract Template */
        const pathContractTemplate = `/Template_Contract/${req.body.Data["gx:providedBy"]}/${CONTRACT_NAME}.docx`;
        const contract = await nextcloudService.getFileFromNextcloud(pathContractTemplate);

        /* Get ODRL */
        const odrl = await contractService.fetchODRL(req.body.Data["gx:termsAndConditions"]);

        /* Update the contract with the odrl */
        const updateContractResult = await contractService.updateContractFromODRL(contract, odrl);
        /* Check if user exists on keycloak and get user Id */
        const userIdConsumer = await keycloakService.getUserKeycloakId(req.body.Consumer.email);
        const userIdProvider = await keycloakService.getUserKeycloakId(req.body.Provider.email);
        if (!userIdConsumer.status || !userIdProvider.status) {
            throw new Error("The Consumer or Provider does not exist on keycloak.");
        }

        /* Creating account for nextcloud if not exists */
        await nextcloudService.addUser(`Keycloak-${userIdConsumer.id}`, req.body.Consumer.email, `${req.body.Consumer.firstName} ${req.body.Consumer.lastName}`);
        await nextcloudService.addUser(`Keycloak-${userIdProvider.id}`, req.body.Provider.email, `${req.body.Provider.firstName} ${req.body.Provider.lastName}`);

        /* Upload of Word document TermsAndConditions in nextcloud */
        const folderName = `${NEGOTIATION_FOLDER}/${req.body.Provider.lastName}_${req.body.Consumer.firstName}-${req.body.Consumer.lastName}_${new Date().toISOString()}`;
        await nextcloudService.uploadFile(updateContractResult.termsAndConditionWord, folderName, `${CONTRACT_NAME}.docx`);

        /* Creation of folder sharing link for provider and consumer */
        await nextcloudService.createShareFolder(folderName, `Keycloak-${userIdConsumer.id}`);
        const shareUrl = await nextcloudService.createShareFolder(folderName, `Keycloak-${userIdProvider.id}`);

        /* Stockage du service offering avec l'id de contract nextcloud */
        await mongoService.put("Credentials", { "id": shareUrl.contractId, "credential": req.body.VerifiableCredential,idProvider:`Keycloak-${userIdProvider.id}`, "data": req.body.Data,"keys":updateContractResult.docXmlKey, isSigned:false});

        /* Send notification mail to the Provider and Consumer */
        mailjetService.sendNegoNotificationMail({ email: req.body.Consumer.email, firstname: req.body.Consumer.firstName, url: shareUrl.url });
        mailjetService.sendNegoNotificationMail({ email: req.body.Consumer.email, firstname: req.body.Provider.firstName, url: shareUrl.url });

        res.status(200).json({ shareUrl: shareUrl.url })
    }
    catch (error) {
        next(new Error(`Error to negociate : ${error}`));
    }
};


exports.startSignature = async (req, res, next) => {
    try {
        /* Récolte des informations de partages pour trouver le consumer */
        const bodyResult=req.body.result;
        const providerId = bodyResult.idUser.replace("Keycloak-", "");
        const consumerId = (await nextcloudService.getUsersIdByShareFolder(`NegociationService/${bodyResult.nameFolder}`)).filter(id => id !== providerId)[0].replace("Keycloak-", "");
        
        /* Récolte des informations du consumer et du provider */
        const dataProvider = await keycloakService.getUserInfo(providerId);
        const dataConsumer = await keycloakService.getUserInfo(consumerId);
        dataProvider["gx-signature:email"] = "yvan-wilfried.mbe-tene@softeam.fr";
        dataConsumer["gx-signature:email"] = "yvan-wilfried.mbe-tene@softeam.fr";

        /* Récupération du service offering dans mongodb et de l'ancien ODRL associé */
        const credentialData = await mongoService.getOne("Credentials", bodyResult.idDocument);

        if(credentialData.idProvider!==bodyResult.idUser){
            res.status(200).json({message:"You are not authorized to start the signing process. Only the supplier is authorized to start the signature process."});
            return;
        }
        if(credentialData.isSigned){
            res.status(200).json({message:"The signature process for this file has already been initiated. Please check your mailbox for further information."});
            return;
        }

        /* Récupération du fichier terms_and_conditions pour générer le nouveau ODRL */
        const termsAndConditions = await nextcloudService.getFileFromNextcloud(`/${NEGOTIATION_FOLDER}/${bodyResult.nameFolder}/${CONTRACT_NAME}.docx`);
        const updateContractOdrl = await contractService.updateOdrlFromContract((await contractService.fetchODRL(credentialData.data["gx:termsAndConditions"])), termsAndConditions);


        if(!utils.areAllKeysInArray(updateContractOdrl.paramContractOdrl,credentialData.keys)){
            res.status(200).json({message:"The data on the form has been deleted or modified. Please ensure that you fill in the information correctly in the relevant forms and do not delete them. In addition, please ensure that all changes have been accepted in the modified areas of the document. Revert to the initial version of the contract if this persists and start the negotiation process again."});
            return;
        }


       const odrlInPdf = await contractService.convertTextToPdf(Buffer.from(JSON.stringify(updateContractOdrl.newOdrl, null, 2)));

        /* Récolte des fichiers dans le dossier partager nextcloud */
        let allFiles = await nextcloudService.getAllFiles(`/${NEGOTIATION_FOLDER}/${bodyResult.nameFolder}`);
        if (allFiles.message) {
            res.status(200).json({message:allFiles.message});
        }
        else {
            allFiles.push({
                fileName: "ODRL.pdf",
                fileBuffer: odrlInPdf
            });
           logger.info(`Successfully download all files for signature : ${bodyResult.nameFolder}`, { allFiles: allFiles });

            /* start Signature */
            const result= await gatewaySignatureService.beginSignature2(allFiles, [dataProvider, dataConsumer], credentialData.credential, updateContractOdrl.newOdrl);
            if (result.message) {
                res.status(200).json(result);
             await mongoService.update("Credentials", credentialData["_id"]);
            } else {
                res.status(200).json({message:'Failed to start signature with the gateway signature try again.'});
            }
        }
    }
    catch (error) {
        console.log(error);
        next(new Error(`Error to start signature : ${error}`));
    }
};

exports.extractODRLVariables = async (req, res) => {
    try {
        // Vérifier si le corps de la requête est au format JSON
        if (req.headers['content-type'] !== 'application/json') {
            return res.status(400).json({ error: 'Invalid content type. Please send JSON data.' });
        }

        const odrlData = req.body;
        res.status(200).json({
            result: utils.flattenJSON(odrlData),
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};

exports.extractODRLData= async (req, res) => {
    try {

        if (req.headers['content-type'] !== 'application/json') {
            return res.status(400).json({ error: 'Invalid content type. Please send JSON data.' });
        }
        const odrlData = req.body;
        res.status(200).json({
            result: utils.reorganizeJSON(odrlData),
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};


