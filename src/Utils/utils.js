function escapeDot(key) {
    // Cette fonction ajoute un caractère d'échappement (\) devant chaque caractère spéciaux dans la clé.
    return key.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

function flattenJSON(input, parentKey = '') {
    let result = {};

    for (const key in input) {
        const newKey = parentKey ? `${parentKey}.${escapeDot(key)}` : escapeDot(key);
        if (typeof input[key] === 'object' && !Array.isArray(input[key])) {
            const nestedResult = flattenJSON(input[key], newKey);
            result = { ...result, ...nestedResult };
        } else if (Array.isArray(input[key])) {
            input[key].forEach((value, index) => {
                if (typeof value === 'object') {
                    const nestedResult = flattenJSON(value, `${newKey}[${index}]`);
                    result = { ...result, ...nestedResult };
                } else {
                    const arrayKey = `${newKey}[${index}]`;
                    result[arrayKey] = value;
                }
            });
        } else {
            result[newKey] = input[key];
        }
    }

    return result;
}

function unescapeDot(key) {
    return key.replace(/\\\\./g, m => m[1]);
}

function splitKey(key) {
    const parts = key.split('.');
    const result = [];

    for (let i = 0; i < parts.length; i++) {
        let currentPart = parts[i];
        while (currentPart.endsWith('\\') && i < parts.length - 1) {
            i++;
            currentPart = currentPart.slice(0, -1) + '.' + parts[i];
        }
        result.push(currentPart.replace(/\\\\/g, '\\'));
    }

    return result;
}

function handleArrayKey(currentResult, arrayKey, realArrayIndex, isLastKey, value) {
    if (!currentResult[arrayKey]) {
        currentResult[arrayKey] = [];
    }
    currentResult = currentResult[arrayKey];

    if (isLastKey) {
        currentResult[realArrayIndex] = value;
    } else if (!currentResult[realArrayIndex]) {
        currentResult[realArrayIndex] = {};
    }

    return currentResult[realArrayIndex];
}

function reorganizeJSON(json) {
    const result = {};

    for (const flatKey in json) {
        const originalKey = unescapeDot(flatKey);
        const keys = splitKey(originalKey);
        let currentResult = result;

        keys.forEach((key, index) => {
            const isLastKey = index === keys.length - 1;

            if (/\[\d+\]$/.test(key)) {
                const [arrayKey, arrayIndex] = key.split('[');
                const realArrayIndex = arrayIndex.slice(0, -1);
                currentResult = handleArrayKey(currentResult, arrayKey, realArrayIndex, isLastKey, json[flatKey]);
            } else {
                if (isLastKey) {
                    currentResult[key] = json[flatKey];
                } else if (!currentResult[key]) {
                    currentResult[key] = {};
                }
                currentResult = currentResult[key];
            }
        });
    }

    return cleanJsonKeys(result);
}

function cleanJsonKeys(obj) {
    if (typeof obj !== 'object' || obj === null) {
        // Si l'objet n'est pas un objet ou est null, ne rien faire
        return obj;
    }
    if (Array.isArray(obj)) {
        // Si c'est un tableau, traiter chaque élément récursivement
        return obj.map(cleanJsonKeys);
    }

    // Si c'est un objet, parcourir les clés et les valeurs
    const cleanedObj = {};
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            const cleanedKey = key.replace(/\\./g, m => m[1]); // Supprime le backslash avant chaque caractère spécial
            const cleanedValue = cleanJsonKeys(obj[key]);
            cleanedObj[cleanedKey] = cleanedValue;
        }
    }

    return cleanedObj;
}


function areAllKeysInArray(tableau1, tableau2) {

        // Vérifier la longueur
        if (tableau1.length !== tableau2.length) {
            return false;
        }

        // Vérifier chaque élément
        for (let i = 0; i < tableau1.length; i++) {
            if (tableau1[i] !== tableau2[i]) {
                return false;
            }
        }

        // Les tableaux sont identiques
        return true;

}

module.exports = {
  flattenJSON,reorganizeJSON,areAllKeysInArray
}