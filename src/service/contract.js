const axios = require('axios');
const PizZip = require('pizzip');
const mammoth = require('mammoth');
const PDFDocument = require('pdfkit');
const puppeteer = require('puppeteer');
const Docxtemplater = require('docxtemplater');
const path = require("path");
const utils=require('../Utils/utils');

async function fetchODRL(TermsAndConditionsUrl) {
    try {
       const response = await axios.get(TermsAndConditionsUrl);
        return response.data;
    }
    catch (error) {
        throw new Error(`Failed to get file ODRL from TermsAndConditions URL ${TermsAndConditionsUrl} : ${error}`);
    }
}

async function updateOdrlFromContract(odrl, contract) {
    // Create a new PizZip instance with the content
    const zip = new PizZip(contract);

    // Extract the document's XML content
    const docXml = zip.files['word/document.xml'].asText();

    // Extract values from Word document
    const dataContract  = extractValuesFromWord(docXml);
    const extractedValues  = dataContract.uniqueOdrlValues;
    // Applaud the ODRL
    const flatODRL = utils.flattenJSON(odrl);
    // Update ODRL with extracted values
    for (let key in flatODRL) {
        if (extractedValues[key]) {
            flatODRL[key] = extractedValues[key];
        }
    }
    const updatedOdrl = utils.reorganizeJSON(flatODRL);
    return {newOdrl:updatedOdrl,paramContractOdrl:dataContract.paramContract};
}

function extractValuesFromWord(docXml) {
    let values = {};
    let allParamContract=[];

    // Regular expression to find tags and extract keys and values
    const tagRegex = /<w:tag w:val="([^"]+)"\s*\/>.*?<w:sdtContent><w:r>(<w:rPr>.*?<\/w:rPr>)?<w:t.*?>(.*?)<\/w:t>/gs;

    let match;
    while ((match = tagRegex.exec(docXml)) !== null) {
        // match[1] contains the key, match[3] contains the value
        values[match[1]] = match[3];
        allParamContract.push(match[1])
    }
    return {uniqueOdrlValues:values,paramContract:allParamContract};
}
async function updateContractFromODRL(contract, odrl) {

    const flatODRL = utils.flattenJSON(odrl);

    const zip = new PizZip(contract);
    let docXml = zip.files['word/document.xml'].asText();

    for (key in flatODRL) {
        docXml = updateXML(docXml, key, flatODRL[key], true);
    }
   const keys=extractKeys(docXml)

    zip.file('word/document.xml', docXml);
    // Enable track changes for the entire document
    const settingsXml = zip.files['word/settings.xml'].asText();
    const updatedSettingsXml = enableTrackChanges(settingsXml);
    zip.file('word/settings.xml', updatedSettingsXml);
    const newDocxContent = zip.generate({ type: 'nodebuffer' });

    return { termsAndConditionWord: newDocxContent,docXmlKey:keys};
}
function extractKeys(xmlResult) {
    const xml2js = require('xml2js');
    let data = [];

    xml2js.parseString(xmlResult, (err, result) => {
        if (err) {
            console.error(err);
            return;
        }
        const wKeyValues = [];
        const extractWKeyValues = (obj) => {
            if (Array.isArray(obj)) {
                obj.forEach((item) => {
                    if (item['$'] && item['$']['w:key']) {
                        wKeyValues.push(item['$']['w:key']);
                    } else {
                        extractWKeyValues(item);
                    }
                });
            } else if (typeof obj === 'object') {
                for (const key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        extractWKeyValues(obj[key]);
                    }
                }
            }
        };

        extractWKeyValues(result);
        // Stocker les valeurs dans data
        data=wKeyValues;
    });
    return data;
}
function enableTrackChanges(settingsXml) {
    // Assuming the presence of a specific setting that controls track changes
    // Modify the XML accordingly to enable track changes globally
    const trackChangesSetting = '<w:trackRevisions w:val="true"/>';
    const regex = /<w:trackRevisions[^>]*\/>/;
    const match = settingsXml.match(regex);

    if (match) {
        // Replace the existing trackRevisions tag with the one enabling track changes
        return settingsXml.replace(regex, trackChangesSetting);
    } else {
        // Insert the trackRevisions tag if it doesn't exist
        const settingsEndTag = '</w:settings>';
        const insertPosition = settingsXml.lastIndexOf(settingsEndTag);
        return settingsXml.slice(0, insertPosition) + trackChangesSetting + settingsXml.slice(insertPosition);
    }
}

function updateXML(xmlString, tagValue, newValue, enableTrackChanges) {
    const escapedTagValue = tagValue.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    const tagRegex = new RegExp(`<w:tag w:val="${escapedTagValue}"\\s*/>.*?<w:sdtContent>(<w:trackRevisions\\s*\\/>|<w:r>(<w:rPr>.*?<\\/w:rPr>)?<w:t.*?>(.*?)<\\/w:t>)`, 'gs');

    return xmlString.replace(tagRegex, (match, p1, p2, p3) => {
        if (enableTrackChanges && p1 && p1.includes('trackRevisions')) {
            const trackChangesContent = `<w:trackRevisions>${p2}${p3.replace(p3, newValue)}</w:trackRevisions>`;
            return match.replace(match, trackChangesContent);
        } else {
            return match.replace(p3, newValue);
        }
    });
}

async function convertWordToPDF(buffer) {
    try {
        const zip = new PizZip(buffer);
        const doc = new Docxtemplater().loadZip(zip);
        doc.setData({});
        doc.render();
        const generatedDoc = doc.getZip().generate({ type: 'nodebuffer' });
        const dataToHtml = await mammoth.convertToHtml({ buffer: generatedDoc });

        let options = {
            headless: "new",
            args: ['--no-sandbox', '--disable-setuid-sandbox']
        };
        if (process.env.NODE_ENV === "production")
            options.executablePath = '/usr/bin/chromium-browser';

        const browser = await puppeteer.launch(options);
        const page = await browser.newPage();
        await page.setContent(dataToHtml.value);
        const pdfBuffer = await page.pdf();
        await browser.close();
        return pdfBuffer;
    } catch (error) {
        throw new Error(`Error to convert word in pdf : ${error}`);
    }
}

    async function  convertTextToPdf(text) {
        return new Promise((resolve, reject) => {
            const doc = new PDFDocument();
            const buffers = [];
            doc.on('data', chunk => {
                buffers.push(chunk);
            });
            doc.on('end', () => {
                const pdfBuffer = Buffer.concat(buffers);
                resolve(pdfBuffer);
            });
            doc.text(text);
            doc.end();
        });
    }


function replaceFileExtension(filename, newExtension) {
    return `${path.basename(filename, path.extname(filename))}.${newExtension}`;
}
module.exports = {
    fetchODRL,
    convertWordToPDF,
    convertTextToPdf,
    updateOdrlFromContract,
    updateContractFromODRL,replaceFileExtension
}
