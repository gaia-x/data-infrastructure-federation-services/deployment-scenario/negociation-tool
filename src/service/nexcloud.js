const path = require('path');
const axios = require('axios');
const logger = require("./logger");
const contract = require("./contract");
const { Client, Server } = require('nextcloud-node-client');
const configSignature = require("../configuration/configSignature");

/* Type de permissions */
const SHARE_PERMISSIONS = {
    READ: 1,
    WRITE: 2,
    CREATE: 4,
    DELETE: 8,
    SHARE: 16
};

/* Type de partage */
const SHARE_TYPE = {
    USER: 0,
    GROUP: 1,
    PUBLIC_LINK: 3,
    EMAIL: 4
};

function getNexcloudClient() {
    const server = new Server({
      basicAuth: {
        username: process.env.NEXTCLOUD_USERNAME,
        password: process.env.NEXTCLOUD_PASSWORD
      },
      url: process.env.NEXTCLOUD_URL,
    });
  
   return new Client(server);
}

async function getFileFromNextcloud(path) {
    try {
        // Get the Nextcloud client instance
        const nextcloudClient = getNexcloudClient();

        // Get the file object from Nextcloud using the specified path
        const file = await nextcloudClient.getFile(path);

        // Check if the file is null (not found)
        if (file == null) {
            throw new Error(`Failed to get file ${path} in Nextcloud. File not found.`);
        }

        // Return the content of the file
        return await file.getContent();
    } catch (error) {
        // Handle errors during the file retrieval process
        console.error(`Failed to get file ${path} in Nextcloud: ${error.message}`);
        throw new Error(`Failed to get file ${path} in Nextcloud: ${error.message}`);
    }
}



async function getAllFiles(folderPath) {
    try {
        const newExtension=[];
        const extensions = configSignature.extensions;
        const nextcloudClient = getNexcloudClient();
        const files = await nextcloudClient.getFiles(folderPath);

        files.forEach(file => {
            newExtension.push(path.extname(file.baseName).slice(1));
        });

        if(!extensions.includes("*")) {
            if (!newExtension.every(element => extensions.includes(element))) {
                const datamessage = `The files sent contain extensions that are not included in the list of authorised extensions. Here are the accepted file extensions: [${extensions.toString()}] and the extensions received: [${newExtension.toString()}]. Please respect these extensions and try again.`;
                return {message: datamessage}
            }
        }
        let filesInfo = [];

        for (const file of files) {
            let fileBuffer;
            let baseName=file.baseName;
            switch (path.extname(file.baseName)) {

                case ".json":
                    baseName = contract.replaceFileExtension(baseName, "pdf");
                    fileBuffer = await contract.convertTextToPdf(await file.getContent());
                    break;
                case ".txt":
                    baseName = contract.replaceFileExtension(baseName, "pdf");
                    fileBuffer = await contract.convertTextToPdf(await file.getContent());
                    break;
                case ".docx":
                    if(baseName==="terms_and_conditions.docx"){
                        baseName = contract.replaceFileExtension(baseName, "pdf");
                        fileBuffer = await contract.convertWordToPDF(await file.getContent());
                    }else{
                        fileBuffer =await file.getContent();
                    }
                    break;
                default:
                    fileBuffer = await file.getContent();
            }
            filesInfo.push({
                "fileName": baseName,
                "fileBuffer": fileBuffer,
            });
        }

        return filesInfo;
    }
    catch (error) {
        throw new Error(`Failed to get file ${path} in nexcloud : ${error}`);
    }
}

async function addUser(id, email, name) {
    try {
        const nextcloudClient = getNexcloudClient();
        const existingNextcloudUser = await nextcloudClient.getUser(id);        
        if (!existingNextcloudUser) {
            const user = await nextcloudClient.createUser({
                id: id,
                email: email
            });
            await user.setDisplayName(name);
        }
        
        if (!existingNextcloudUser) logger.info(`User ${email} added successfully`);
    } catch (error) {
        throw new Error(`Failed to add user ${name} in nexcloud : ${error}`);
    }
}


async function uploadFile(fileBuffer, folderName, fileName) {
    try {
        const nextcloudClient = getNexcloudClient();
        const shareFolder = await nextcloudClient.createFolder(folderName);
        await shareFolder.createFile(fileName, fileBuffer);
    } catch (error) {
        throw new Error(`Failed to upload file in nextcloud: ${error}`);
    }
}

async function createShareFolder(folderPath, userId) {
    try {
        const client = axios.create({
            baseURL: process.env.NEXTCLOUD_URL,
            auth: {
                username: process.env.NEXTCLOUD_USERNAME,
                password: process.env.NEXTCLOUD_PASSWORD
            }
        });

        const ocsAddress = `${client.defaults.baseURL}/ocs/v2.php/apps/files_sharing/api/v1/shares?format=json`;
        const data = {
            path: folderPath,
            shareWith: userId,
            shareType: SHARE_TYPE.USER,
            permissions: SHARE_PERMISSIONS.READ | SHARE_PERMISSIONS.WRITE | SHARE_PERMISSIONS.CREATE | SHARE_PERMISSIONS.DELETE | SHARE_PERMISSIONS.SHARE
        };

        const responseShareFolder = await client.post(ocsAddress, data, { headers: { 'OCS-APIRequest': 'true' } });

        const shareUrl = `${client.defaults.baseURL}/index.php/apps/files/?dir=${responseShareFolder.data.ocs.data.file_target}&fileid=${responseShareFolder.data.ocs.data.file_source}`;
        const contractId = responseShareFolder.data.ocs.data.file_source;
      logger.info('Successfully created share folder', { "URL": shareUrl, "contractId": contractId });

        return {
            url: shareUrl,
            contractId: contractId
        };
    }
    catch (error) {
        console.log(error)
        throw new Error(`Failed to create share folder ${folderPath} for userId ${userId}: ${error}`);
    }
}

async function getUsersIdByShareFolder(folderPath) {
    try {
        const client = axios.create({
            baseURL: process.env.NEXTCLOUD_URL,
            auth: {
                username: process.env.NEXTCLOUD_USERNAME,
                password: process.env.NEXTCLOUD_PASSWORD
            }
        });
    
        const ocsAddress = `${client.defaults.baseURL}/ocs/v2.php/apps/files_sharing/api/v1/shares?format=json&path=${encodeURI(folderPath)}&reshares=true`;
        const responseShareFolder = await client.get(ocsAddress, { headers: { 'OCS-APIRequest': 'true' } });
    
        return [responseShareFolder.data.ocs.data[0].share_with, responseShareFolder.data.ocs.data[1].share_with]
    }
    catch (error) {
        throw new Error(`Failed to get users id for folder ${folderPath}: ${error}`);
    }
}

module.exports = {
    addUser,
    uploadFile,
    getAllFiles,
    createShareFolder,
    getFileFromNextcloud,
    getUsersIdByShareFolder
}