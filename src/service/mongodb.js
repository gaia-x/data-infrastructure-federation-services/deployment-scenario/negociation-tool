const logger = require('./logger');
const { MongoClient,ObjectId } = require('mongodb');

let client;
let config = {
    "connectionString" : `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@${process.env.MONGO_SERVICE_HOST}:${process.env.MONGO_SERVICE_PORT}/`,
    "dbName": "NegociationTool",
    // "dbName": "Template",
    "dbCollections": ["Credentials"],
   "defaultMongoDB": "mongodb://adminuser:password123@localhost:27019/?authMechanism=DEFAULT",
 //"defaultMongoDB": "mongodb://localhost:27017"
}

async function init() {
    try {
        client = new MongoClient((typeof process.env.MONGO_INITDB_ROOT_USERNAME != 'undefined') ? config.connectionString : config.defaultMongoDB);
        await client.connect();
        const db = client.db(config.dbName);
        for (const collectionName of config.dbCollections) {
            const collections = await db.listCollections({ name: collectionName }).toArray();
            if (collections.length === 0) {
                await db.createCollection(collectionName);
                console.log(`La collection ${collectionName} a été crée avec succès.`);
            }
        }
        logger.info("Successful connected to mongo");
    } catch (error) {
        console.error('Une erreur s\'est produite lors de la création de la collection:', error); 
        process.exit(1);
    }
}

async function put(collectionName, row) {
    try {
        //  insert a document into the specified collection based on its ID
        await client.db(config.dbName).collection(collectionName).replaceOne(
            { "id": row.id },
            row,
            { upsert: true }
        );
    } catch (error) {
        // Handle errors during the insert process
        console.error(`Error updating/inserting document with ID ${row.id} in collection ${collectionName}: ${error.message}`);
        throw new Error(`Failed to update/insert document: ${error.message}`);
    }
}

async function update(collectionName, id) {
    try {
        // Convert the string ID to ObjectId for the filter
        const filter = { _id: new ObjectId(id) };

        // Define the update operation: set the 'isSigned' field to true
        const update = { $set: { isSigned: true } };

        // Perform the update operation on the specified collection
        const result = await client.db(config.dbName).collection(collectionName).updateOne(filter, update);

        // Check if exactly one document was modified during the update
        return  result.modifiedCount === 1 ? true  /* The update was successful*/ : false  /*No document was modified, indicating the document with the specified ID was not found*/;
    } catch (error) {
        // Handle errors during the update process
        console.error('Error during update:', error);
        throw new Error(`Failed to update document: ${error.message}`);
    }
}


async function deleteOne(collectionName, id) {
    try {
        // Delete a document from the specified collection based on its ID
        await client.db(config.dbName).collection(collectionName).deleteOne({ "id": id });
    } catch (error) {
        // Handle errors during the deletion process
        console.error(`Error deleting document with ID ${id} from collection ${collectionName}: ${error.message}`);
        throw new Error(`Failed to delete document: ${error.message}`);
    }
}

async function getOne(collectionName, id) {
    try {
        // Retrieve a document from the specified collection based on its ID
        const obj = await client.db(config.dbName).collection(collectionName).findOne({ "id": id });

        // Check if the document exists
        return (obj != null) ?  obj  : { message: `No object with ID ${id} found in collection ${collectionName}` } /* If the document does not exist, return an informative message */;
    } catch (error) {
        // Handle errors during the retrieval process
        console.error(`Error retrieving document with ID ${id} from collection ${collectionName}: ${error.message}`);
        throw new Error(`Failed to retrieve document: ${error.message}`);
    }
}

module.exports = { init, put, deleteOne, getOne,update};