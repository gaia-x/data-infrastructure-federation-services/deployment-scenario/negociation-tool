const fs = require('fs');
const axios = require("axios");
const logger = require('./logger');
const FormData = require('form-data');
const { Readable } = require('stream');
const contractService = require("../service/contract");
const dataSignature = require("../data/CredentialRequestSignature.json");

async function beginSignature(pdfBuffer, fileName, signatoriesData, odrlData, dataProduct) {

    try {
        /* Build data for CredentialRequestSignature for digital signature gateway */
        dataSignature["credentialSubject"]["gx-signature:signatories"] = signatoriesData;
        dataSignature["credentialSubject"]["gx-signature:attachments"] = [
            {
                "gx-signature:documentId": Math.floor(Math.random() * 100000) + 1,
                "gx-signature:documentName": fileName,
                "gx-signature:name": fileName,
                "gx-signature:isToSign": "true",
            },
            {
                "gx-signature:documentId": Math.floor(Math.random() * 100000) + 1,
                "gx-signature:documentName": "Odrl.pdf",
                "gx-signature:name": "Odrl.pdf",
                "gx-signature:isToSign": "true",
            }
        ];

        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:providedBy"] = signatoriesData[0]["gx-signature:did"];
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:consumedBy"] = signatoriesData[1]["gx-signature:did"];
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:termOfUsage"] = "https://example.com/terms-of-usage";
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:dataUsage"] = "https://example.com/data-usage";
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:notarizedIn"] = "https://example.com/notarizedIn";
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:dataProduct"] = dataProduct;
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["odrl:hasPolicy"] = odrlData.Data;

        /* Build files for digital signature gateway */
        const formData = new FormData();
        formData.append("TermsAndConditions", pdfBuffer, { filename: fileName });
        formData.append('odrlInPDF', await contractService.convertTextToPdf(Buffer.from(JSON.stringify(odrlData.Data, null, 2))), { filename: "Odrl.pdf"});
        formData.append('CredentialRequestSignature', Readable.from([JSON.stringify(dataSignature, null, 2)]), { filename: 'CredentialRequestSignature.json'});
        
        /* Call signature gateway */
        const headers = {
            "Content-Type": "multipart/form-data",
            "X-API-KEY": process.env.X_API_KEY,
            ...formData.getHeaders(),
        };
      await axios.post(`${process.env.SIGNATURE_GATEWAY_URL}/contralia/dsc/did:web:agdatahub.provider.demo23.gxfs.fr/signature`, formData, { headers });
    } 
    catch (error) {
        throw new Error(`Failed to start signature with the gateway signature: ${error}`);
    }
}

async function beginSignature2(pdfArray, signatoriesData, dataProduct, odrl) {
    try {
        /* Build credentialRequestSignature.json */
        dataSignature["credentialSubject"]["gx-signature:signatories"] = signatoriesData;

        for (file of pdfArray) {
            dataSignature["credentialSubject"]["gx-signature:attachments"].push({
                "gx-signature:documentId": Math.floor(Math.random() * 100000) + 1,
                "gx-signature:documentName": file.fileName,
                "gx-signature:name": file.fileName,
                "gx-signature:isToSign": "true",
            });
        }

        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:providedBy"] = signatoriesData[0]["gx-signature:did"];
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:consumedBy"] = signatoriesData[1]["gx-signature:did"];
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:termOfUsage"] = "https://example.com/terms-of-usage";
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:dataUsage"] = "https://example.com/data-usage";
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:notarizedIn"] = "https://example.com/notarizedIn";
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["gx:dataProduct"] = dataProduct;
        dataSignature["credentialSubject"]["gx-signature:signature"]["gx-signature:subject"]["odrl:hasPolicy"] = odrl.Data;

       // logger.info("New Signature Request", { credentialRequestSignature: dataSignature });

        /* Build files for digital signature gateway */
        const formData = new FormData();
        formData.append('CredentialRequestSignature', Readable.from([JSON.stringify(dataSignature, null, 2)]), { filename: 'CredentialRequestSignature.json'});

        for (file of pdfArray) {
            formData.append(file.fileName, file.fileBuffer, { filename: file.fileName});
        }

        const headers = {
            "Content-Type": "multipart/form-data",
            "X-API-KEY": process.env.X_API_KEY,
            ...formData.getHeaders()
        };

        await axios.post(`${process.env.SIGNATURE_GATEWAY_URL}/contralia/dsc/did:web:agdatahub.provider.demo23.gxfs.fr/signature`, formData, { headers });

      logger.info('Successful signature start');
        return { message: 'Successful signature start' }
    }
     catch (error) {
        console.error(`Failed to start signature with the gateway signature: ${error}`)
        return {} ;
    }
}

module.exports = {
    beginSignature,
    beginSignature2
}
