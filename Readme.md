
# Negotiation Tool prototype
# Sommaire

1. [Introduction](#introduction)
2. [Description of the operating scenario](#description)
3. [General information](#information)
4. [Architecture](#architecture)
5. [Entry point](#entrypoint)
6. [Adapting a Contract Model Using ODRL Data](#adapting-a-contract-model-using-odrl-data)
7. [API NextcloudTool](#api-nextcloudtool)
8. [OnlyOffice](#onlyoffice)
9. [The process of creating an ODRL contract](#odrlcontract)
10. [Notification process](#notification-process)
11. [Authentication and user management](#authentication-and-user-management)
12. [Technologies and tools used](#technology-and-tool)
13. [Step to install the project locally](#step-to-install-the-project-locally)
14. [Authors and acknowledgment](#authors-and-acknowledgment)
15. [Licence](#licence)


## Introduction <a name="introduction"></a>
The negotiation workflow is used to define the process for a Consumer to negotiate or subscribe to a service in the Federated Catalogue, with the aim of formalising the contractualisation of services between the various parties.
Thanks to this negotiation workflow, we have established a programme enabling a Consumer to negotiate or subscribe to a data service in the Federated Catalogue. This process authorises a Consumer to connect to the catalogue using their electronic wallet to subscribe to services. The negotiation workflow acts as an intermediary step between the selection of a data service in the Federated Catalogue and the signature process.
The services available are listed in the Federated Catalogue, which you can consult by following this link: https://webcatalog.abc-federation.gaia-x.community/catalog.
Once on the catalogue, Consumers can browse the various services and their descriptions.To subscribe to or negotiate one of these services, Consumers need to log in using their wallet. For the connection, we used keycloak and the abc-federation wallet.

### Scheme workflow
![Texte alternatif](./image/architecture1.png)

Below is an image describing the different services in the catalogue:

### Overview of services
![Texte alternatif](./image/Catalogue2.png)

### Description of services and actions to be taken
![Texte alternatif](./image/catalogue1.png)

## Description of the operating scenario <a name="description"></a>

The negotiation process takes place between a Provider and a Consumer for a service in the federated catalogue. The aim is to enable the two parties to reach agreement via the negotiation tool on the terms and conditions of use of the service.

For more information about the negotiation workflow service please refer to this documentation : https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/main/scenarii_details.md#dataproduct-discovered-by-employee-of-the-data-consummer

To implement the service contractualisation, we have set up a negotiation tool. This negotiation tool is used to formalise the contractualisation of a data service between the two parties. The negotiation is launched at the federated catalogue level via the negotiation button.
This negotiation tool, as implemented, could be replaced by a tool made available by a DataSpace.

When the negotiation process begins, all the information concerning the Consumer and the Provider is collected and integrated into the negotiation process.

As far as the signature plugin is concerned, it is up to each electronic signature provider to create its own plugin. In our case, we are using the proprietary Contralia signature solution.

To make the negotiation process between the two parties a reality, we used Nextcloud for the negotiation and file exchange part, and MailJet for the notification part by sending emails to inform the various parties of the opening of the negotiation.

The tool we are making available can be integrated into the negotiation and emailing tools provided by each DataSpace, subject to a few modifications.


## General information <a name="information"></a>

In this project we have decided to use Nextcloud as the negotiation tool.
Nextcloud is an open-source software that provides a platform for file synchronization, sharing, and collaboration. It allows users to host their own private cloud storage service, ensuring data privacy and control. The platform supports extensions that add functionalities such as calendar, contacts, and task management. Users can access their data through a web interface, desktop clients, or mobile apps. Its architecture is modular, allowing for easy integration with other services and tools.

As part of the negotiation process, the terms and conditions of the services to be negotiated are defined in Odrl format.
You can see this information in red in the contract to be negotiated.

![Texte alternatif](./image/nextcloud1.png)

For information on the data format associated with ODRL, please click on the link below:
https://gitlab.com/gaia-x/technical-committee/federation-services/data-exchange/-/blob/main/specs/dewg.md

To mark the end of the negotiation between the different parties, the Provider can start the signature process directly 
on the nextcloud by clicking on the start signature button, using our Nextcloud plugin to initiate the start of the signature gateway with the recovery of the negotiated documents.

![Texte alternatif](./image/nextcloud2.png)

### Operational diagram of the signature module integrated into the negotiation process

![Texte alternatif](./image/plugin.png)

The Nextcloud plugin programme can be accessed by clicking on the link below :
https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/nextcloud

## Architecture <a name="architecture"></a>

The workflow architecture is described below. For more information, please see the link below:
https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/scenario-23.11/-/blob/main/NegotiationProcessWorkflowProposal.md?plain=0

![Texte alternatif](./image/architecture.png)


### Entry point <a name="entrypoint"></a>

The tool has an entry point which is an exposed route for the Federated Catalogue to send the  formatted contract. This route depends of the third party choice but MUST be known from the catalogue and accept an harmonized data format.  
The body request must be done following a unique template, as following : 

```
POST https://negociation-tool.abc-federation.gaia-x.community/initiate-negotiation
ARG json object

{
  "Consumer": {
    "nom": "Alice",
    "prenom": "EmployeeCredential",
    "email": "yvan-wilfried.mbe-tene@softeam.fr",
    "username": "did:key:z6mkodg6shf41sj4gpqetz1v33obcmutsv6qakmdg3pa638z"
  },
  "Provider": {
    "nom": "from Dufour Storage",
    "prenom": "Bob",
    "email": "bobfromdufour@yahoo.fr",
    "username": "Dufour Storage"
  },
  "Data": {
    "nomService": "dufour",
    "gx:consumedBy": "did:key:z6mkodg6shf41sj4gpqetz1v33obcmutsv6qakmdg3pa638z",
    "gx:dataProduct": "Gestion des services cloud",
    "gx-service-offering:serviceType": "Data Product",
    "gx:policies": "https://dufourstorage.provider.gaia-x.community/policies",
    "gx:termsAndConditions": "http://localhost:3002/initiate-negotiation/getOdrlService?name=odrl_dufour.json",
    "gx:providedBy": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/legalperson/data.json"
  },
  "Type": "Subscribe"
}
```
"The data includes information about the Consumer, which must be provided on a mandatory basis."
```
  "Consumer": {
    "nom": "Alice",
    "prenom": "EmployeeCredential",
    "email": "yvan-wilfried.mbe-tene@softeam.fr",
    "username": "did:key:z6mkodg6shf41sj4gpqetz1v33obcmutsv6qakmdg3pa638z"
  },
```

"The data includes information about the Provider, which must be provided on a mandatory basis." 
```
  "Provider": {
    "nom": "from Dufour Storage",
    "prenom": "Bob",
    "email": "bobfromdufour@yahoo.fr",
    "username": "Dufour Storage"
  }
```

"Data contains information related to services."
```
"Data": {
    "nomService": "dufour",
    "gx:consumedBy": "did:key:z6mkodg6shf41sj4gpqetz1v33obcmutsv6qakmdg3pa638z",
    "gx:dataProduct": "Gestion des services cloud",
    "gx-service-offering:serviceType": "Data Product",
    "gx:policies": "https://dufourstorage.provider.gaia-x.community/policies",
    "gx:termsAndConditions": "http://localhost:3002/initiate-negotiation/getOdrlService?name=odrl_dufour.json",
    "gx:providedBy": "did:web:dufourstorage.provider.gaia-x.community:participant:1225ed1cc7755bc5867413f1ca1cf99eb66d4f7a148e10e346390c4bb302f649/vc/legalperson/data.json"
  },
```
"The file must include the type of action, which is either a subscription or a negotiation."

```
  "Type": "Subscribe"
```



Data in the body will be used for many purposes : 
-	Aknowledge both parties information (this will be later used for users management / notification).
-	Create a shareable contract on which both parties will work on.
- the link contained in the gx:termsAndConditions field contains an example of the terms and conditions of the contract in odrl, so here is an example of the content:
````
{
  "Data": {
    "@context": "https://www.w3.org/ns/odrl.jsonld",
    "@type": "Set",
    "uid": "https://votre-entreprise.com/policy:1",
    "nomService": "dufour",
    "permission": [
      {
        "target": "https://votre-entreprise.com/dataset/TechFin-2023",
        "assignee": "Data Subscriber",
        "action": "use",
        "duty": [
          {
            "assigner": "Data Provider Inc.",
            "assignee": "Data Subscriber",
            "action": [
              {
                "value": "pay",
                "refinement": [
                  {
                    "leftOperand": "payAmount",
                    "operator": "eq",
                    "rightOperand": {
                      "@value": "75.00",
                      "@type": "xsd:decimal"
                    },
                    "unit": "https://dbpedia.org/resource/Euro"
                  }
                ]
              }
            ]
          },
          {
            "action": "nextPolicy",
            "target": "https://votre-entreprise.com/policy:1010"
          }
        ],
        "constraint": [
          {
            "leftOperand": "odrl:spatial",
            "operator": "isAnyOf",
            "rightOperand": {
              "@list": [
                "Europe",
                "États-Unis"
              ]
            }
          },
          {
            "leftOperand": "odrl:industry",
            "operator": "isAnyOf",
            "rightOperand": {
              "@list": [
                "Technologie"
              ]
            }
          },
          {
            "leftOperand": "odrl:product",
            "operator": "isAnyOf",
            "rightOperand": {
              "@list": [
                "Données Financières"
              ]
            }
          },
          {
            "leftOperand": "dateTime",
            "operator": "lt",
            "rightOperand": {
              "@value": "2025-09-15",
              "@type": "xsd:date"
            }
          }
        ]
      }
    ],
    "obligation": [
      {
        "target": "https://votre-entreprise.com/dataset/TechFin-2023",
        "assigner": "Data Subscriber",
        "assignee": "Data Provider Inc.",
        "action": "distribute"
      }
    ]
  }
}
````

# Adapting a Contract Model Using ODRL Data  <a name="adapting-a-contract-model-using-odrl-data"></a>

The general terms and conditions in ODRL are drawn up by the supplier from a contract template that incorporates the variables extracted from the ODRL file, which can be modified. Below is an example of a contract template that includes the variables taken from the ODRL general terms and conditions file, which can be adjusted.

Below is an example of a contract template using Odrl variables:

![Texte alternatif](./image/template_contrat_odrl.png)



Below Scheme for the Integration of ODRL Data from the Catalogue during Subscription or Trading into the Trading File for Addition to the Trading Area between the Parties Involved :

![Texte alternatif](./image/add-odrl-to-contrat.png)

## API NextcloudTool <a name="api-nextcloudtool"></a>
The API NextcloudTool provides the following routes :

1. {url} : The url of the API NextcloudTool

| HTTP Verb | url                                           | input               | output | Description                                                  |
|-----------|-----------------------------------------------|---------------------|-|--------------------------------------------------------------|
| HTTP POST | https://{URL}/initiate-negotiation/ | application/json    | application/json  | starts the process of negotiating or subscribing to services |
| HTTP POST | https://{URL}/initiate-negotiation/dataSpace | application/json    | application/json | Allows you to connect a new dataspace                        |
| HTTP POST | https://{URL}/initiate-negotiation/startSignature | multipart/form-data | application/json  | Start the signing process (signature)                          |
| HTTP GET  | https://{URL}/initiate-negotiation/getOdrlService | n/a                 | application/json  | Retrieving terms and conditions in odrl format                          |


The solution must imperatively include a call to the signature gateway, a tool that operates autonomously. That's how we integrate it into our workflow to enable documents negotiated by the various parties to be signed in nextcloud.

The Digital Signature Gateway provides the following api :

1. {service} : The name of signature service
2. {did}     : The name of the Digital Signature Consumer (DSC)

| HTTP Verb | url | input | output | Description |
|---|---|---|---|---|
| HTTP POST | https://{service}/DSC/{did}/signature |  multipart/form-data request with CredentialRequestSignature VC and all documents to include in signature | The CredentialResponseSignature VC | Initiate a new contract (signature) |

The documents to be sent MUST be in PDF format and can be the following :
- The contract (mandatory)
- Any related documents

For more information on the signature of the gateway, please click on the link below :
https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/digital-signature-gateway

### CredentialRequestSignature VC

The CredentialRequestSignature VC MUST be forged by the third party and MUST follow the following template : 
```
"@context": [
   "https://www.w3.org/2018/credentials/v1",
   "https://www.gaiax.eu/registry/signature/v1"
 ],
 "id": "http://prodider.eu/contract/9999999",
 "type": [
   "VerifiableCredential",
   "CredentialRequestSignature"
 ],
 "issuer": "did:web:dufourstorage.provider.gaia-x.community",
 "issuanceDate": "2010-01-01T00:00:00Z",
 "credentialSubject": {
   "gx-signature:request": {
     "gx-signature:messageBody": "text of the email",
     "gx-signature:id": "did:web:cloudprovider.eu",
     "gx-signature:urlcallback": "https://contracts.provider.eu/contract",
     "gx-signature:expirationDate": "2023-10-01T00:00:00Z",
     "gx-signature:redirectParticipantTo": "https://webcatalog.abc-federation.dev.gaiax.ovh/catalog/",
     "gx-signature:signatureLevel": "Simple"
   },
   "gx-signature:signatories": [
     {
       "gx-signature:userid": "Keycloak-3e6d0625-4b79-4bd6-bfdc-ad8bb2eae1f2",
       "gx-signature:email": "yvan-wilfried.mbe-tene@softeam.fr",
       "gx-signature:did": "Dufour Storage",
       "gx-signature:firstname": "Bob",
       "gx-signature:lastname": "from Dufour Storage",
       "gx-signature:mobile": "0646273476"
     },
     {
       "gx-signature:userid": "Keycloak-e553a474-ef58-4839-89ba-bb0a577269db",
       "gx-signature:email": "yvan-wilfried.mbe-tene@softeam.fr",
       "gx-signature:did": "did:key:z6mkodg6shf41sj4gpqetz1v33obcmutsv6qakmdg3pa638z",
       "gx-signature:firstname": "EmployeeCredential",
       "gx-signature:lastname": "Alice",
       "gx-signature:mobile": "0646273476"
     }
   ],
   "gx-signature:allowedReaders": [
     {
       "gx-signature:did": "did:web:cloudprovider.fr:legalentity:legalbot",
       "gx-signature:mobile": "+345664533"
     },
     {
       "gx-signature:email": "bot_cloudprovider@cloudprovider.eu",
       "gx-signature:did": "did:web:cloudprovider.fr:legalentity:legalbot",
       "gx-signature:mobile": "+345664533"
     },
     {
       "gx-signature:email": "alice.@cloudprovider.eu",
       "gx-signature:did": "did:web:did.consummer.eu:users:alice",
       "gx-signature:mobile": "+33456765432"
     }
   ],
   "gx-signature:attachments": [
     {
       "gx-signature:documentId": 9325,
       "gx-signature:documentName": "term_and_condition.pdf",
       "gx-signature:name": "term_and_condition.pdf",
       "gx-signature:isToSign": "true"
     },
     {
       "gx-signature:documentId": 11073,
       "gx-signature:documentName": "OdrlData.pdf",
       "gx-signature:name": "OdrlData.pdf",
       "gx-signature:isToSign": "true"
     }
   ],
   "gx-signature:signature": {
     "gx-signature:subject": {
       "odrlData": {
         "Data": {
           "@context": "https://www.w3.org/ns/odrl.jsonld",
           "@type": "Set",
           "uid": "https://votre-entreprise.com/policy:1",
           "nomService": "dufour",
           "permission": [
             {
               "target": "https://votre-entreprise.com/dataset/TechFin-2023",
               "assignee": "Data Subscriber",
               "action": "use",
               "duty": [
                 {
                   "assigner": "Data Provider Inc.",
                   "assignee": "Data Subscriber",
                   "action": [
                     {
                       "value": "pay",
                       "refinement": [
                         {
                           "leftOperand": "payAmount",
                           "operator": "eq",
                           "rightOperand": {
                             "@value": "75.00",
                             "@type": "xsd:decimal"
                           },
                           "unit": "https://dbpedia.org/resource/Euro"
                         }
                       ]
                     }
                   ]
                 },
                 {
                   "action": "nextPolicy",
                   "target": "https://votre-entreprise.com/policy:1010"
                 }
               ],
               "constraint": [
                 {
                   "leftOperand": "odrl:spatial",
                   "operator": "isAnyOf",
                   "rightOperand": {
                     "@list": [
                       "Europe",
                       "États-Unis"
                     ]
                   }
                 },
                 {
                   "leftOperand": "odrl:industry",
                   "operator": "isAnyOf",
                   "rightOperand": {
                     "@list": [
                       "Technologie"
                     ]
                   }
                 },
                 {
                   "leftOperand": "odrl:product",
                   "operator": "isAnyOf",
                   "rightOperand": {
                     "@list": [
                       "Données Financières"
                     ]
                   }
                 },
                 {
                   "leftOperand": "dateTime",
                   "operator": "lt",
                   "rightOperand": {
                     "@value": "2025-09-15",
                     "@type": "xsd:date"
                   }
                 }
               ]
             }
           ],
           "obligation": [
             {
               "target": "https://votre-entreprise.com/dataset/TechFin-2023",
               "assigner": "Data Subscriber",
               "assignee": "Data Provider Inc.",
               "action": "distribute"
             }
           ]
         }
       }
     },
     "gx-signature:schemas": [
       {
         "gx-signature:schema": "http://cloudprovider/contractSchema.JSONLD",
         "gx-signature:acronym": "CP",
         "gx-signature:type": "cloudproviderContract"
       }
     ]
   }
 },
 "proof": {}
 }
 )
```

For more information about the digital signature tool, please refer to this documentation https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/digital-signature-gateway

### OnlyOffice <a name="onlyoffice"></a>

OnlyOffice is an office suite that provides users with tools to create, edit, and collaborate on text documents, spreadsheets, and presentations directly from a web browser. It handle documents in .docx format. The .docx format is used in our current implementation.
OnlyOffice integration in Nextcloud allows users to edit office files directly from the Nextcloud interface, making it easier to collaborate on documents without leaving the Nextcloud environment. 
This integration is crucial for our negociationTool demonstrator.

### The process of creating an ODRL contract <a name="odrlcontract"></a>

Welcome to the process of creating an ODRL (Open Digital Rights Language) contract. To create this type of file, follow these key steps:

1. **DOCXF file creation:**
   First, create a DOCXF file with the name "terms_and_conditions" . This document will serve as the basis for your ODRL contract. You can use software such as Only Office, Microsoft Word or Google Docs for this step.

2. **Adding content to the contract:**
   Integrate the contents of your contract into the DOCXF file. Please include all details and clauses necessary to define the terms and conditions of the agreement.

3. **Add text fields:** Identify specific data to be negotiated.
   Identify specific data to be negotiated later in the contract. At these points, create text fields. These text fields will serve as markers for variable information in the contract.

4. **Obtain variable information in ODRL:**
   Send your ODRL file, containing the terms and conditions to be negotiated, as the body of the HTTP POST request to the URL https://negociation-tool.aster-x.demo23.gxfs.fr/extract-odrl-data. This will provide you with the variables you need to fill in the text fields in your DOCXF file.

5. **ODRL variable mapping:**
   Associate each text field with an ODRL variable. Ensure that the key of each text field corresponds to the appropriate ODRL variable.

6. **Save file as DOCX:**
   Save the modified DOCXF file as DOCX. Make sure that all ODRL variables have been correctly integrated into the document.

7. **Add to "template_contract/" folder:**
   Save your DOCX file in the specified folder, usually called "template_contract". This will enable you to organize your contracts and find them easily.


### Notification process <a name="notification-process"></a>

Both parties are automatically informed of the opening of the negotiation via an email sent by Nextcloud. In order to retrieve the email, it MUST be in the LegalPerson credential used in the authentication process.
Once the negotiation is closed, the Gaia-X signature service will inform both parties of the signature process.
 
### Authentication and user management <a name="authentication-and-user-management"></a>

In our example, and for all third-parties, the negotiation tool MUST implement the Keycloak OIDC connector in order to :
- Get information linked to the federations's user such as providers and consumers informations via the LegalPerson VC.
- Simplify the login process. The Keycloak realm is shared with the Federated catalogue, meaning when initiating a negotiation through the catalogue, the user will not have to login again when he is redirected to the negotiation tool.

In order for a user to connect to the Keycloak instance via it's SSI wallet, a VP request asking for a LegalPerson and Participant credential is sent.

# Technologies and tools used <a name="technology-and-tool"></a>

### Programming languages used

1.  NodeJs
2.  Reactjs

### The tools used
1.  Nextcloud
2.  MongoDb
3.  OnlyOffice
4.  MailJet
5.  Keycloak


### Technology
The technology used for the nextcloud plugin is in nodejs.

### Step to install the project locally <a name="step-to-install-the-project-locally"></a>

Currently our Nextcloud solution is available only locally. No deployment has been done so far.

####  Step 1

Create a docker network and deploy Nextcloud  
```
docker network create nextcloud_network
```
```
docker run -d \
  --name nextcloud \
  --network nextcloud_network \
  -p 8080:80 \
  -v /opt/nextcloud-data:/var/www/html/data
  -e MYSQL_HOST=mariadb \
  -e MYSQL_ROOT_PASSWORD=root_password \
  -e MYSQL_PASSWORD=nextcloud_db_password \
  -e MYSQL_DATABASE=nextcloud \
  nextcloud

```
Deploy OnlyOffice in the same network
```
docker run -i -t -d --name onlyoffice \
  -p 80:80 -p 443:443 \
  -e JWT_ENABLED=true \
  -e JWT_SECRET=your_jwt_secret \
  -e JWT_HEADER=AuthorizationJwt \
  -e JWT_ISSUER=your_issuer \
  -e MYSQL_SERVER_HOST=mysql \
  -e MYSQL_SERVER_PORT=3306 \
  -e MYSQL_SERVER_DB_NAME=onlyoffice \
  -e MYSQL_SERVER_USER=onlyoffice \
  -e MYSQL_SERVER_PASS=your_mysql_password \
  --network nextcloud_network \
  onlyoffice/communityserver
```
##### "Insert a private key in place of 'your_jwt_secret'."
####  Step 2

Create the administrator profile in nextcloud (follow the GUI instructions)

Integrate the information relating to the IP address of the machine hosting the Nextcloud and OnlyOffice server into Nextcloud's /var/www/html/config/config.php file :
```
'trusted_domains' =>
array (
0 => '192.168.22.126:8080',
1 => '192.168.22.126:80',
2 => '127.0.0.1:8080',
3 => '127.0.0.1:80',
)
```
"Please run the following command to grant the necessary permissions to the Nextcloud configuration file:
```
chmod a+rwx /var/www/html/config/config.php
```

Then restart Nextcloud."

####   Step 3 

Integrate OnlyOffice with Nextcloud:

1.  In Nextcloud, go to the "Apps" section.
2.  Find and install the "ONLYOFFICE" connector app.
3.  After installing, go to the Nextcloud settings and find the ONLYOFFICE section.
4.  Enter the address of the OnlyOffice Document Server. Since they are on the same Docker network, you can use the container name as a hostname. Therefore, you would enter `http://onlyoffice` as the Document Server address.

![Texte alternatif](./image/onlyoffice.png)

Now, Nextcloud should be able to communicate directly with OnlyOffice.

####   Step 4 
Integration of the Social login plugin to allow users to access their negotiation space via keycloak using their wallet

1.  **Install the Social Login App**:
    -   In the Nextcloud interface, go to "Apps", search for "Social login", download and enable the app.
2.  **Configure Social Login App**:
    -   In the Nextcloud settings, under "Administration", find the "Social login" section.
    -   Use the "OpenID Connect" provider option:
        -   **Nom interne**:  "Keycloak".
        -   **Fonction** : "Keycloak",
        -   **URL d'authorisation** : https://keycloak.abc-federation.dev.gaiax.ovh/realms/FederatedCatalog/protocol/openid-connect/auth
        -    **URL de jeton**  :  https://keycloak.abc-federation.dev.gaiax.ovh/realms/FederatedCatalog/protocol/openid-connect/token
        -   **Id client**: FederatedCatalog
        -   **Secret du client**: 1pJTR4eIFHN1Pye8TuO0rbKjC2ugrSwE
        - **Portée** : "opened"

![Texte alternatif](./image/sociallogin.png)

3. **Configure Social Login App in Keycloak**:

![Texte alternatif](./image/keycloak.png)

####   Step 5
**Integration of the signature plugin in nextcloud**:
1. Configure the signature plugin by following the link below:
   https://apps.nextcloud.com/developer/apps/generate
2. In the Nextcloud "/var/www/html/apps/" directory, integrate the plugin by downloading its contents from the following link:
   https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/nextcloud

## Authors and acknowledgment <a name="authors-and-acknowledgment"></a>
GXFS-FR

## License <a name="licence"></a>
The Negotiation Tool example is delivered under the terms of the Apache License Version 2.0.


